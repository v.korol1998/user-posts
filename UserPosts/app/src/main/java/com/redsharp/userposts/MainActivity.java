package com.redsharp.userposts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.widget.AutoCompleteTextView;

import com.redsharp.userposts.items.User;
import com.redsharp.userposts.ui.posts.PostsFragment;
import com.redsharp.userposts.ui.users.UsersFragment;
import com.redsharp.userposts.ui.users.OnUserClickListener;

public class MainActivity extends AppCompatActivity implements OnUserClickListener {

    Fragment current;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        fragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            showUsers();
        }
    }

    private void showUsers(){
        UsersFragment usersFragment = new UsersFragment(this);
        current = usersFragment;
        fragmentManager.beginTransaction()
                .replace(R.id.container, usersFragment)
                .commitNow();
    }

    @Override
    public void OnUserClick(User user) {
        current = new PostsFragment(user);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, current)
                .commitNow();
    }

    @Override
    public void onBackPressed() {
        if (current instanceof PostsFragment) {
            showUsers();
        } else {
            super.onBackPressed();
        }
    }
}
