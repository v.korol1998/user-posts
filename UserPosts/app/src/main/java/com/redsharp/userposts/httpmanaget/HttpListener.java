package com.redsharp.userposts.httpmanaget;

public interface HttpListener {

    void OnAnswer(String s);

    void OnFail(String s);
}
