package com.redsharp.userposts.httpmanaget;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class HttpManager extends AsyncTask<String, Void, String> {

    private HttpListener httpListener;

    private HttpManager(String url, HttpListener httpListener){
        this.execute(url);
        this.httpListener = httpListener;
    }

    public static HttpManager newHttpIntent(String url, HttpListener httpListener){
        return new HttpManager(url, httpListener);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s.length() > 0) {
            httpListener.OnAnswer(s);
        } else {
            httpListener.OnFail(s);
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        String str = strings[0];
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        try {
            URL url = new URL(str);
            urlConn = url.openConnection();
            InputStreamReader in = new InputStreamReader(urlConn.getInputStream());
            bufferedReader = new BufferedReader(in);
            StringBuilder stringBuffer = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuffer.append(line);
            }
            in.close();
            bufferedReader.close();
            return stringBuffer.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
