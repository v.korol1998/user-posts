package com.redsharp.userposts.httpmanaget;

import android.location.Geocoder;

import com.redsharp.userposts.items.Address;
import com.redsharp.userposts.items.Comment;
import com.redsharp.userposts.items.Company;
import com.redsharp.userposts.items.Geo;
import com.redsharp.userposts.items.Post;
import com.redsharp.userposts.items.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class Parser {

    private Parser(){ }

    public static Parser newInstance(){
        return new Parser();
    }

    public List<User> parseUsers(String str){
        LinkedList<User> users = new LinkedList<>();
        try{
            JSONArray jsonArray = new JSONArray(str);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject o = jsonArray.getJSONObject(i);
                User user = new User();
                user.setId(o.getInt("id"));
                user.setName(o.getString("name"));
                user.setEmail(o.getString("email"));
                user.setUsername(o.getString("username"));
                user.setPhone(o.getString("phone"));
                user.setWebsite(o.getString("website"));

                JSONObject com = o.getJSONObject("company");
                user.setCompany(new Company(com.getString("name"),
                                            com.getString("catchPhrase"),
                                            com.getString("bs")));

                JSONObject addr = o.getJSONObject("address");
                JSONObject geo = addr.getJSONObject("geo");
                user.setAddress(new Address(addr.getString("street"),
                                            addr.getString("suite"),
                                            addr.getString("city"),
                                            addr.getString("zipcode"),
                                            new Geo(geo.getString("lat"),
                                                    geo.getString("lng"))));
                users.add(user);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return users;
    }

    public List<Post> parsePosts(String str){
        LinkedList<Post> posts = new LinkedList<>();
        try {
            JSONArray jsonArray = new JSONArray(str);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject o = jsonArray.getJSONObject(i);
                Post post = new Post(o.getInt("userId"),
                                     o.getInt("id"),
                                     o.getString("title"),
                                     o.getString("body"));
                posts.add(post);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return posts;
    }

    public List<Comment> parseComments(String str){
        LinkedList<Comment> comments = new LinkedList<>();
        try {
            JSONArray jsonArray = new JSONArray(str);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject o = jsonArray.getJSONObject(i);
                Comment comment = new Comment(o.getInt("postId"),
                                              o.getInt("id"),
                                              o.getString("name"),
                                              o.getString("email"),
                                              o.getString("body"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return comments;
    }
}
