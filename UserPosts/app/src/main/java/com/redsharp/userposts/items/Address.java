package com.redsharp.userposts.items;

import android.location.Geocoder;

import androidx.annotation.NonNull;

public class Address {

    private String street;
    private String suite;
    private String city;
    private String zipcode;
    private Geo geocoder;

    public Address() {
    }

    public Address(String street, String suite, String city, String zipcode, Geo geo) {
        this.street = street;
        this.suite = suite;
        this.city = city;
        this.zipcode = zipcode;
        this.geocoder = geo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuite() {
        return suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Geo getGeocoder() {
        return geocoder;
    }

    public void setGeocoder(Geo geocoder) {
        this.geocoder = geocoder;
    }

    @NonNull
    @Override
    public String toString() {
        return city + " " + street + " " + suite;
    }
}
