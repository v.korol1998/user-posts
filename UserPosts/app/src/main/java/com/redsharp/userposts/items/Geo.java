package com.redsharp.userposts.items;

public class Geo {

    private String lgn;
    private String lat;

    public Geo() {
    }

    public Geo(String lat, String lgn) {
        this.lgn = lgn;
        this.lat = lat;
    }

    public String getLon() {
        return lgn;
    }

    public void setLgn(String lgn) {
        this.lgn = lgn;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }
}
