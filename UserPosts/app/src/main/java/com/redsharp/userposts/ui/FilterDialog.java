package com.redsharp.userposts.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.redsharp.userposts.R;
import com.redsharp.userposts.ui.filter.FilterCallback;

import java.util.List;

public class FilterDialog extends DialogFragment implements View.OnClickListener {

    private FilterCallback filterCallback;
    private List<String> strings;
    private Spinner spinner;
    private AutoCompleteTextView textView;

    public FilterDialog(FilterCallback filterCallback, List<String> strings) {
        super();
        this.filterCallback = filterCallback;
        this.strings = strings;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.filter_dialog, container, false);
        spinner = v.findViewById(R.id.spinner);
        spinner.setAdapter(new ArrayAdapter<String>(getContext(),
                            android.R.layout.simple_spinner_item,
                            new String[]{"Non", "ABC", "CBA"}));
        textView = v.findViewById(R.id.autoCompleteTextView);
        textView.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, strings));
        Button filter = v.findViewById(R.id.filter);
        filter.setOnClickListener(this);
        Button cancel = v.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return v;
    }

    @Override
    public void onClick(View view) {
        String res = "";
        String s = textView.getText().toString();
        Log.d("s", s);
        if(!s.equals("")){
            res = s;
        }
        switch(spinner.getSelectedItem().toString()){
            case "Non":
                filterCallback.filter(res, FilterCallback.Sort.NON);
                break;
            case "ABC":
                filterCallback.filter(res, FilterCallback.Sort.ABC);
                break;
            case "CBA":
                filterCallback.filter(res, FilterCallback.Sort.CBA);
                break;
        }
        dismiss();
    }
}
