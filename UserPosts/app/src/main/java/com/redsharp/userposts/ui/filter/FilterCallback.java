package com.redsharp.userposts.ui.filter;

public interface FilterCallback {

    enum Sort{
        ABC, CBA, NON
    }

    void filter(String name, Sort sort);

}
