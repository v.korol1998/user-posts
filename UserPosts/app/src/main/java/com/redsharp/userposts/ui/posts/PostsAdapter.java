package com.redsharp.userposts.ui.posts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.redsharp.userposts.R;
import com.redsharp.userposts.httpmanaget.HttpListener;
import com.redsharp.userposts.httpmanaget.HttpManager;
import com.redsharp.userposts.httpmanaget.Parser;
import com.redsharp.userposts.items.Post;
import com.redsharp.userposts.items.User;
import com.redsharp.userposts.ui.filter.FilterCallback;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostViewHolder> implements HttpListener {

    private Context context;
    private List<Post> postsKnown;
    private List<Post> postsShown;
    private User user;

    public PostsAdapter(Context context, User user) {
        postsKnown = new LinkedList<>();
        postsShown = new LinkedList<>();
        this.user = user;
        this.context = context;
        HttpManager.newHttpIntent("https://jsonplaceholder.typicode.com/posts?userId=" + user.getId(), this);
    }

    public void showBySorts(String s, FilterCallback.Sort sort){
        if(s.equals("")){
            postsShown = postsKnown;
        } else {
            List<Post> _posts = new LinkedList<>();
            for (Post post : postsKnown)
                if(post.getTitle().contains(s))
                    _posts.add(post);
            postsShown = _posts;
        }
        switch (sort){
            case ABC:
                showByABC();
                break;
            case CBA:
                showByCBA();
                break;
            default:
                notifyDataSetChanged();
        }
    }

    private void showByABC(){
        Collections.sort(postsShown, new Comparator<Post>() {
            @Override
            public int compare(Post user, Post t1) {
                return user.getTitle().compareTo(t1.getTitle());
            }
        });
        notifyDataSetChanged();
    }

    private void showByCBA(){
        Collections.sort(postsShown, new Comparator<Post>() {
            @Override
            public int compare(Post user, Post t1) {
                return t1.getTitle().compareTo(user.getTitle());
            }
        });
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.post_card, parent, false);
        return new PostViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {
        holder.bind(postsShown.get(position));
    }

    @Override
    public int getItemCount() {
        return postsShown.size();
    }

    @Override
    public void OnAnswer(String s) {
        postsKnown = Parser.newInstance().parsePosts(s);
        postsShown = postsKnown;
        notifyDataSetChanged();
    }

    @Override
    public void OnFail(String s) {
        Toast.makeText(context, "Fail " + s, Toast.LENGTH_SHORT).show();
    }

    public List<String> getPostsTitles() {
        List<String> strings = new LinkedList<>();
        for (Post post : postsKnown) {
            strings.add(post.getTitle());
        }
        return strings;
    }

    public static class PostViewHolder extends RecyclerView.ViewHolder{

        private TextView title;
        private TextView bode;

        public PostViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            bode = itemView.findViewById(R.id.body);
        }

        public void bind(Post p){
            title.setText(p.getTitle());
            bode.setText(p.getBody());
        }
    }
}
