package com.redsharp.userposts.ui.posts;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.redsharp.userposts.R;
import com.redsharp.userposts.items.Post;
import com.redsharp.userposts.items.User;
import com.redsharp.userposts.ui.FilterDialog;
import com.redsharp.userposts.ui.filter.FilterCallback;
import com.redsharp.userposts.ui.posts.PostsAdapter;
import com.redsharp.userposts.ui.users.OnUserClickListener;
import com.redsharp.userposts.ui.users.UserAdapter;

public class PostsFragment extends Fragment implements FilterCallback, View.OnClickListener {

    private RecyclerView recyclerView;
    private Context context;
    private User user;
    private PostsAdapter adapter;

    public PostsFragment(User user){
        this.user = user;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.main_fragment, container, false);
        context = getContext();
        recyclerView = v.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new PostsAdapter(context, user);
        recyclerView.setAdapter(adapter);
        FloatingActionButton button = v.findViewById(R.id.floatingActionButton);
        button.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {
        FilterDialog filterDialog = new FilterDialog(this, adapter.getPostsTitles());
        filterDialog.show(getChildFragmentManager(), "dialog");
    }

    @Override
    public void filter(String name, FilterCallback.Sort sort) {
        adapter.showBySorts(name, sort);
    }
}
