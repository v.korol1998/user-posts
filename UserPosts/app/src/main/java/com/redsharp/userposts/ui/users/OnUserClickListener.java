package com.redsharp.userposts.ui.users;

import com.redsharp.userposts.items.User;

public interface OnUserClickListener {

    void OnUserClick(User user);

}
