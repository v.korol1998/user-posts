package com.redsharp.userposts.ui.users;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.redsharp.userposts.R;
import com.redsharp.userposts.httpmanaget.HttpListener;
import com.redsharp.userposts.httpmanaget.HttpManager;
import com.redsharp.userposts.httpmanaget.Parser;
import com.redsharp.userposts.items.User;
import com.redsharp.userposts.ui.filter.FilterCallback;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> implements HttpListener {

    private OnUserClickListener onUserClickListener;
    private Context context;
    private List<User> usersKnown;
    private List<User> usersShown;

    public List<String> getUsersUsernames(){
        LinkedList<String> usernames = new LinkedList<>();
        for (User user : usersKnown) {
            usernames.add(user.getUsername());
        }
        return usernames;
    }

    public void showBySorts(String s, FilterCallback.Sort sort){
        if(s.equals("")){
            usersShown = usersKnown;
        } else {
            List<User> _users = new LinkedList<>();
            for (User user : usersKnown)
                if(user.getUsername().contains(s))
                    _users.add(user);
                usersShown = _users;
        }
        switch (sort){
            case ABC:
                showByABC();
                break;
            case CBA:
                showByCBA();
                break;
            default:
                notifyDataSetChanged();
        }
    }

    private void showByABC(){
        Collections.sort(usersShown, new Comparator<User>() {
            @Override
            public int compare(User user, User t1) {
                return user.getUsername().compareTo(t1.getUsername());
            }
        });
        notifyDataSetChanged();
    }

    private void showByCBA(){
        Collections.sort(usersShown, new Comparator<User>() {
            @Override
            public int compare(User user, User t1) {
                return t1.getUsername().compareTo(user.getUsername());
            }
        });
        notifyDataSetChanged();
    }

    public UserAdapter(Context context, OnUserClickListener onUserClickListener) {
        this.context = context;
        this.onUserClickListener = onUserClickListener;
        usersShown = new LinkedList<>();
        usersKnown = new LinkedList<>();
        startFinder();
    }

    private void startFinder(){
        HttpManager.newHttpIntent("https://jsonplaceholder.typicode.com/users", this);
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.user_card, parent, false);
        return new UserViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        holder.bind(usersShown.get(position), onUserClickListener);
    }

    @Override
    public int getItemCount() {
        return usersShown.size();
    }

    @Override
    public void OnAnswer(String s) {
        usersKnown = Parser.newInstance().parseUsers(s);
        usersShown = usersKnown;
        notifyDataSetChanged();
    }

    @Override
    public void OnFail(String s) {
        Toast.makeText(context, "Fail " + s, Toast.LENGTH_SHORT).show();
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView username;
        TextView mail;
        TextView address;
        CardView card;

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            username = itemView.findViewById(R.id.userName);
            mail = itemView.findViewById(R.id.email);
            address = itemView.findViewById(R.id.address);
            card = itemView.findViewById(R.id.card);
        }

        public void bind(final User user, final OnUserClickListener onUserClickListener){
            name.setText(user.getName());
            username.setText(user.getUsername());
            mail.setText(user.getEmail());
            address.setText(user.getAddress().toString());
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onUserClickListener.OnUserClick(user);
                }
            });
        }
    }
}
