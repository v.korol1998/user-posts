package com.redsharp.userposts.ui.users;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.redsharp.userposts.R;
import com.redsharp.userposts.ui.FilterDialog;
import com.redsharp.userposts.ui.filter.FilterCallback;

public class UsersFragment extends Fragment implements View.OnClickListener, FilterCallback {

    private RecyclerView recyclerView;
    private Context context;
    private OnUserClickListener onUserClickListener;
    private UserAdapter adapter;

    public UsersFragment(OnUserClickListener onUserClickListener){
        this.onUserClickListener = onUserClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.main_fragment, container, false);
        context = getContext();
        recyclerView = v.findViewById(R.id.recycler);
        FloatingActionButton fab = v.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new UserAdapter(context, onUserClickListener);
        recyclerView.setAdapter(adapter);
        return v;
    }

    @Override
    public void onClick(View view) {
        FilterDialog filterDialog = new FilterDialog(this, adapter.getUsersUsernames());
        filterDialog.show(getChildFragmentManager(), "dialog");
    }

    @Override
    public void filter(String name, Sort sort) {
        adapter.showBySorts(name, sort);
    }
}
